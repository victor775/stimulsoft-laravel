<?php

return [
    // DB connection name from database config
    'connection' => 'mysql',

    // Stimulsoft license key
    'license_key' => '',
];
