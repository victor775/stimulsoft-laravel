## Описание

Пакет для работы с js версией stimulsoft 2018. Содержит в себе все ассеты необходимые для работы

## Установка

1. Подключить этот пакет через composer
    ```json
    "require": {
        "smartcats/stimulsoft-laravel": "0.1"
    },
    "repositories": [
        {
            "type": "git",
            "url": "git@gitlab.com:victor775/stimulsoft-laravel.git"
        }
    ]
    ```
2. Запустите команду
    ```bash
    php artisan vendor:pusblish --tag=stimulsoft
    ```

3. Создайте роут для handler
4. В контроллере создайте handler и определите логику сохранения отчета в соответсвующем колбеке
    ```php
    <?php
    $handler = new StiHandler();

    $handler->registerErrorHandlers();

    $handler->onBeginProcessData = function ($event) {
        return StiResult::success();
    };

    $handler->onPrintReport = function ($event) {
        return StiResult::success();
    };

    $handler->onBeginExportReport = function ($event) {
        return StiResult::success();
    };

    $handler->onEndExportReport = function ($event) {
        return StiResult::success();
    };

    $handler->onEmailReport = function ($event) {
        $event->settings->from = "******@gmail.com";
        $event->settings->host = "smtp.gmail.com";
        $event->settings->login = "******";
        $event->settings->password = "******";
    };

    $handler->onDesignReport = function ($event) {
        return StiResult::success();
    };

    $handler->onCreateReport = function ($event) {
        return StiResult::success();
    };

    $handler->onSaveReport = function ($event) {
        return StiResult::error(trans('crud.access_denied'));
    };

    $handler->onSaveReport = function ($event) use ($report) {
       $reportJson = $event->reportJson; // Report JSON
       $fileName = $event->fileName; // Report file name
       $report->name = $fileName;
       $report->text = $reportJson;
       $report->save();

       return StiResult::success(trans('crud.saved'));
   };
    ```
5. Подключите вьюхи из пакета туда, где необходимо вывести отчет ($reportJson - текст отчета в json формате. $route - url хендлера)
    ```blade
        <!-- Для дизайнера отчетов -->
        @include('stimulsoft::sti-helper', ['handler' => $route])
        @include('stimulsoft::designer-init', ['reportJson' => $reportJson ?? null])
        <div id="designerContent"></div>
    ```
    ```blade
        <!-- Для вьювера отчетов -->
        @include('stimulsoft::sti-helper', ['handler' => $route])
        @include('stimulsoft::viewer-init', ['reportJson' => $reportJson ?? null])
        <div id="viewerContent"></div>
    ```
    
## Пример (с обфускацией запросов в режиме просмотра (viewer))

*StimulsoftManager (Сервисный класс)*
```php
<?php

namespace App\Services\Reports\Stimulsoft;

use App\Models\Report\Report;
use SmartCats\Stimulsoft\Classes\StiResult;
use SmartCats\Stimulsoft\QueryHider;
use SmartCats\Stimulsoft\StiHandler;

class StimulsoftManager
{
    public function processDesigner(Report $report)
    {
        $handler = $this->createHandler();

        $handler->onSaveReport = function ($event) use ($report) {
            $reportJson = $event->reportJson; // Report JSON
            $fileName = $event->fileName; // Report file name
            $report->name = $fileName;
            $report->text = $reportJson;
            $report->save();

            return StiResult::success(trans('crud.saved'));
        };

        return $handler->process();
    }

    public function processViewer(Report $report)
    {
        $handler = $this->createHandler();

        $handler->onBeginProcessData = function ($event) use ($report) {
            $queriesMap = QueryHider::getMap($report->text);

            if (isset($event->queryString)) {
                $query = QueryHider::getQueryFromString($event->queryString);

                if (array_key_exists($query, $queriesMap)) {
                    $event->queryString = str_replace($query, $queriesMap[$query], $event->queryString);
                } else {
                    unset($event->queryString);
                }
            }
        };

        return $handler->process();
    }

    private function createHandler()
    {
        $handler = new StiHandler();

        $handler->registerErrorHandlers();

        $handler->onBeginProcessData = function ($event) {
            return StiResult::success();
        };

        $handler->onPrintReport = function ($event) {
            return StiResult::success();
        };

        $handler->onBeginExportReport = function ($event) {
            return StiResult::success();
        };

        $handler->onEndExportReport = function ($event) {
            return StiResult::success();
        };

        $handler->onEmailReport = function ($event) {
            $event->settings->from = "******@gmail.com";
            $event->settings->host = "smtp.gmail.com";
            $event->settings->login = "******";
            $event->settings->password = "******";
        };

        $handler->onDesignReport = function ($event) {
            return StiResult::success();
        };

        $handler->onCreateReport = function ($event) {
            return StiResult::success();
        };

        $handler->onSaveReport = function ($event) {
            return StiResult::error(trans('crud.access_denied'));
        };

        $handler->onSaveAsReport = function ($event) {
            return StiResult::error(trans('crud.access_denied'));
        };

        return $handler;
    }
}
```

*Controller*
```php
    <?php

    public function show($id)
    {
        $report = $this->getQuery()->findOrFail($id);
    
        if (!$this->hasUserAccess($report)) {
            return redirect()->route($this->getRoutePrefix().'.index')
                ->with('flash_danger', trans('crud.access_denied'));
        }
    
        $manager = app(StimulsoftManager::class);
    
        $reportJson = QueryHider::hide($report->text);
    
        return view('report.reports.viewer')
            ->with('page_title', $this->getPageTitle())
            ->with('manager', $manager)
            ->with('report', $report)
            ->with('reportJson', $reportJson);
    }
    
    public function edit($id)
    {
        $report = $this->getQuery()->findOrFail($id);
    
        $manager = app(StimulsoftManager::class);
    
        return view('report.reports.designer')
            ->with('page_title', $this->getPageTitle())
            ->with('manager', $manager)
            ->with('report', $report)
            ->with('reportJson', $report->text);
    }
    
    public function create()
    {
        $manager = app(StimulsoftManager::class);
    
        return view('report.reports.designer')
            ->with('manager', $manager)
            ->with('page_title', $this->getPageTitle());
    }
    
    public function designerHandler(StimulsoftManager $manager, $id = null)
    {
        $report = $this->getNewModelInstance();
    
        if ($id) {
            $report = $this->getQuery()->findOrFail($id);
        }
    
        return $manager->processDesigner($report);
    }
    
    public function viewerHandler($id, StimulsoftManager $manager)
    {
        $report = $this->getQuery()->findOrFail($id);
    
        if (!$this->hasUserAccess($report)) {
            return [
                'success' => false,
                'notice'  => trans('crud.access_denied'),
            ];
        }
    
        return $manager->processViewer($report);
    }
```

*Роуты*
```php
    <?php
    Route::post('reports/designer-handler/{id?}', 'ReportController@designerHandler')
            ->name('reports.handler-designer');
    
    Route::post('reports/viewer-handler/{id}', 'ReportController@viewerHandler')
    ->name('reports.handler-viewer');
```

*Вьюхи*
```blade
@extends('layouts.app')

@section('title', $report->name)

@section('head')
    @include('stimulsoft::sti-helper', ['handler' => route('report.reports.handler-viewer', $report->id), 'localePath' => asset('stimulsoft/js/localization/' . app()->getLocale() . '.xml')])
@endsection

@section('body')
    <div id="viewerContent"></div>
@endsection

@section('scripts')
    @include('stimulsoft::viewer-init', ['reportJson' => $reportJson])
@endsection
```

```blade
@extends('layouts.app')

@section('title', $page_title)

@section('head')
    @include('stimulsoft::sti-helper', ['handler' => route('report.reports.handler-designer', $report->id ?? null), 'localePath' => asset('stimulsoft/js/localization/' . app()->getLocale() . '.xml')])
@endsection

@section('body')
    <div id="designerContent"></div>
@endsection

@section('scripts')
    @include('stimulsoft::designer-init', ['reportJson' => $reportJson ?? null])
@endsection
```
