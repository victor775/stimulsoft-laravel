<!-- Report Office2013 style -->
<link href="{{ asset('stimulsoft/css/stimulsoft.viewer.office2013.whiteteal.css') }}" rel="stylesheet">
<link href="{{ asset('stimulsoft/css/stimulsoft.designer.office2013.lightgrayteal.css') }}" rel="stylesheet">
<!-- Stimusloft Reports.JS -->
<script src="{{ asset('stimulsoft/js/stimulsoft.reports.js') }}" type="text/javascript"></script>
<script src="{{ asset('stimulsoft/js/stimulsoft.viewer.js') }}" type="text/javascript"></script>
<script src="{{ asset('stimulsoft/js/stimulsoft.designer.js') }}" type="text/javascript"></script>

<script type="text/javascript">
  StiHelper.prototype.process = function (args, callback) {
    if (args) {
      if (args.event == "BeginProcessData") {
        args.preventDefault = true;
        if (args.database == "XML" || args.database == "JSON") return callback(null);
      }
      var command = {};

      for (var p in args) {
        if (p == "report" && args.report != null) command.report = JSON.parse(args.report.saveToJsonString());
        else if (p == "settings" && args.settings != null) command.settings = args.settings;
        else if (p == "data") command.data = Stimulsoft.System.Convert.toBase64String(args.data);
        else command[p] = args[p];
      }

      var json = JSON.stringify(command);
      if (!callback) callback = function (message) {
        if (Stimulsoft.System.StiError.errorMessageForm && !String.isNullOrEmpty(message)) {
          var obj = JSON.parse(message);
          if (!obj.success || !String.isNullOrEmpty(obj.notice)) {
            var message = String.isNullOrEmpty(obj.notice) ? "There was some error" : obj.notice;
            Stimulsoft.System.StiError.errorMessageForm.show(message, obj.success);
          }
        }
      }
      jsHelper.send(json, callback);
    }
  }

  StiHelper.prototype.send = function (json, callback) {
    try {
      var request = new XMLHttpRequest();
      request.open("post", this.url, true);
      request.timeout = this.timeout * 1000;
      request.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'));
      request.onload = function () {

        if (request.status == 200) {
          var responseText = request.responseText;
          request.abort();
          callback(responseText);
        }
        else {
          Stimulsoft.System.StiError.showError("[" + request.status + "] " + request.statusText, false);
        }
      };

      request.onerror = function (e) {
        var errorMessage = "Connect to remote error: [" + request.status + "] " + request.statusText;
        Stimulsoft.System.StiError.showError(errorMessage, false);
      };
      request.send(json);
    }
    catch (e) {
      var errorMessage = "Connect to remote error: " + e.message;
      Stimulsoft.System.StiError.showError(errorMessage, false);
      request.abort();
    }
  };

  StiHelper.prototype.getUrlVars = function (json, callback) {
    var vars = {};
    var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi,
      function (m, key, value) {
        vars[key] = value;
      });
    return vars;
  }

  StiHelper.prototype.disableDataSources = function () {
    for (var key in StiOptions.Services.databases) {
      if( ["Firebird", "Oracle", "PostgreSQL", "MS SQL"].indexOf(StiOptions.Services.databases[key].serviceName) !== -1 ) {
            StiOptions.Services.databases[key].serviceEnabled = false;
        }
    }
  }

  StiHelper.prototype.getLicense = function () {
    Stimulsoft.Base.StiLicense.key = '{{ config('stimulsoft.license_key') }}';
  }

  StiHelper.prototype.getLocale = function () {
    var request = new XMLHttpRequest();
    request.open("get", "{{ $localePath ?? asset('stimulsoft/js/localization/ru.xml') }}", false);
    request.onload = function () {
      if (request.status == 200) {
        Stimulsoft.Base.Localization.StiLocalization.setLocalization(request.responseText);
      }
      else {
        Stimulsoft.System.StiError.showError("[" + request.status + "] " + request.statusText, false);
      }
    };
    request.onerror = function (e) {
      var errorMessage = "Connect to remote error: [" + request.status + "] " + request.statusText;
      Stimulsoft.System.StiError.showError(errorMessage, false);
    };

    request.send();
  }

  function StiHelper(url, timeout) {
    this.url = url;
    this.timeout = timeout;
    this.disableDataSources();
    this.getLicense();
    this.getLocale();
  }

  jsHelper = new StiHelper("{{ $handler }}", {{ $timeout ?? 600 }});
</script>
