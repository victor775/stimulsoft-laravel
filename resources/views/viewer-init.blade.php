<script type="text/javascript">
  var options = new Stimulsoft.Viewer.StiViewerOptions();
  options.appearance.fullScreenMode = true;
  options.toolbar.showSendEmailButton = true;

  var viewer = new Stimulsoft.Viewer.StiViewer(options, "StiViewer", false);

  // Process SQL data source
  viewer.onBeginProcessData = function (event, callback) {
        jsHelper.process(arguments[0], arguments[1]);
  }

  // Manage export settings on the server side
  viewer.onBeginExportReport = function (args) {

  }
  // Send exported report to Email
  viewer.onEmailReport = function (event) {
        jsHelper.process(arguments[0], arguments[1]);
  }

  // Load and show report
  var report = new Stimulsoft.Report.StiReport();
  report.load({!! $reportJson !!});
  viewer.report = report;
  viewer.renderHtml("{{ $container ?? 'viewerContent' }}");
</script>

