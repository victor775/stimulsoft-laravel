<script type="text/javascript">
  var options = new Stimulsoft.Designer.StiDesignerOptions();
  options.appearance.fullScreenMode = true;
  options.toolbar.showSendEmailButton = true;

  var designer = new Stimulsoft.Designer.StiDesigner(options, "StiDesigner", false);

  // Process SQL data source
  designer.onBeginProcessData = function (event, callback) {
    jsHelper.process(arguments[0], arguments[1]);
  }

  // Save report template on the server side
  designer.onSaveReport = function (event) {
    jsHelper.process(arguments[0], arguments[1]);
  }

  // Load and design report
  var report = new Stimulsoft.Report.StiReport();
  @isset($reportJson)
    report.load({!! $reportJson !!});
  @endisset
  designer.report = report;
  designer.renderHtml("{{ $container ?? 'designerContent' }}");
</script>
