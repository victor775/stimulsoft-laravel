<?php

namespace SmartCats\Stimulsoft;

use Illuminate\Support\ServiceProvider as LaravelServiceProvider;

class ServiceProvider extends LaravelServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes([
            __DIR__.'/../config/stimulsoft.php' => config_path('stimulsoft.php'),
        ], 'stimulsoft');

        $this->publishes([
            __DIR__.'/../assets' => public_path('stimulsoft'),
        ], 'stimulsoft');

        $this->publishes([
            __DIR__.'/../resources/views' => resource_path('views/vendor/stimulsoft'),
        ], 'stimulsoft');

        $this->loadViewsFrom(__DIR__.'/../resources/views', 'stimulsoft');
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
