<?php

namespace SmartCats\Stimulsoft\Adapters;

use SmartCats\Stimulsoft\Classes\StiResult;
use Illuminate\Database\QueryException;
use Exception;
use DB;

class StiMySqlAdapter
{
    private $connection;

    public function __construct()
    {
        $this->connection = config('stimulsoft.connection');
    }

    public function parse($connectionString)
    {
        return true;
    }

    public function test()
    {
        return StiResult::success();
    }

    public function execute($queryString)
    {
        $result = StiResult::success();

        try {
            $parsedQuery = $this->parseQuery($queryString);
        } catch (Exception $e) {
            return StiResult::error('Query syntax error. Format should be like - SELECT * FROM table; ("var":"{value}", "var2":"{value2}")');
        }

        try {
            $data = DB::connection($this->connection)->select($parsedQuery['query'], $parsedQuery['bindings']);
        } catch (QueryException $e) {
            return StiResult::error($e->getMessage());
        }

        $result->columns = isset($data[0]) ? array_keys((array)$data[0]) : [];
        $result->types = array_fill(0, count($result->columns), 'string');
        $result->rows = [];

        foreach ($data as $row) {
            $result->rows[] = array_values((array)$row);
        }

        return $result;
    }

    private function parseQuery($queryString)
    {
        $parameters = explode(';', $queryString, 2);

        $bindings = [];

        if (isset($parameters[1])) {
            $bindings = json_decode(str_replace(['(', ')'], ['{', '}'], $parameters[1]), true);

            $bindings = $bindings ?? [];
        }

        $result = [
            'query'    => $parameters[0],
            'bindings' => $bindings,
        ];

        return $result;
    }
}
