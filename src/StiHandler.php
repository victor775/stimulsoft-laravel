<?php

namespace SmartCats\Stimulsoft;

use SmartCats\Stimulsoft\Adapters\StiMySqlAdapter;
use SmartCats\Stimulsoft\Classes\StiDatabaseType;
use SmartCats\Stimulsoft\Classes\StiEventType;
use SmartCats\Stimulsoft\Classes\StiExportFormat;
use SmartCats\Stimulsoft\Classes\StiRequest;
use SmartCats\Stimulsoft\Classes\StiResult;
use SmartCats\Stimulsoft\Classes\StiResponse;
use SmartCats\Stimulsoft\Classes\StiEmailSettings;
use stdClass;

class StiHandler
{
    private function checkEventResult($event, $args)
    {
        if (isset($event)) {
            $result = $event($args);
        }
        if (!isset($result)) {
            $result = StiResult::success();
        }
        if ($result === true) {
            return StiResult::success();
        }
        if ($result === false) {
            return StiResult::error();
        }
        if (gettype($result) == "string") {
            return StiResult::error($result);
        }
        if (isset($args)) {
            $result->object = $args;
        }

        return $result;
    }

    private function getQueryParameters($query)
    {
        $result = [];
        while (strpos($query, "{") !== false) {
            $query = substr($query, strpos($query, "{") + 1);
            $parameterName = substr($query, 0, strpos($query, "}"));
            $result[$parameterName] = null;
        }

        return $result;
    }

    private function applyQueryParameters($query, $values)
    {
        $result = "";
        while (strpos($query, "{") !== false) {
            $result .= substr($query, 0, strpos($query, "{"));
            $query = substr($query, strpos($query, "{") + 1);
            $parameterName = substr($query, 0, strpos($query, "}"));
            if (isset($values) && isset($values[$parameterName]) && !is_null($values[$parameterName])) {
                $result .= strval($values[$parameterName]);
            } else {
                $result .= "{".$parameterName."}";
            }
            $query = substr($query, strpos($query, "}") + 1);
        }

        return $result.$query;
    }

//--- Events

    public $onBeginProcessData = null;

    private function invokeBeginProcessData($request)
    {
        $args = new stdClass();
        $args->sender = $request->sender;
        $args->database = $request->database;
        $args->connectionString = $request->connectionString;
        $args->queryString = $request->queryString;
        if (isset($request->queryString)) {
            $args->parameters = $this->getQueryParameters($request->queryString);
        }

        $result = $this->checkEventResult($this->onBeginProcessData, $args);
        if (isset($result->object->queryString) && isset($args->parameters)) {
            $result->object->queryString = $this->applyQueryParameters($result->object->queryString, $args->parameters);
        }

        return $result;
    }

    public $onEndProcessData = null;

    private function invokeEndProcessData($request, $result)
    {
        $args = new stdClass();
        $args->sender = $request->sender;
        $args->result = $result;

        return $this->checkEventResult($this->onEndProcessData, $args);
    }

    public $onCreateReport = null;

    private function invokeCreateReport($request)
    {
        $args = new stdClass();
        $args->sender = $request->sender;

        return $this->checkEventResult($this->onCreateReport, $args);
    }

    public $onOpenReport = null;

    private function invokeOpenReport($request)
    {
        $args = new stdClass();
        $args->sender = $request->sender;

        return $this->checkEventResult($this->onOpenReport, $args);
    }

    public $onSaveReport = null;

    private function invokeSaveReport($request)
    {
        $args = new stdClass();
        $args->sender = $request->sender;
        $args->report = $request->report;
        $args->reportJson = $request->reportJson;
        $args->fileName = $request->fileName;

        return $this->checkEventResult($this->onSaveReport, $args);
    }

    public $onSaveAsReport = null;

    private function invokeSaveAsReport($request)
    {
        $args = new stdClass();
        $args->sender = $request->sender;
        $args->report = $request->report;
        $args->reportJson = $request->reportJson;
        $args->fileName = $request->fileName;

        return $this->checkEventResult($this->onSaveAsReport, $args);
    }

    public $onPrintReport = null;

    private function invokePrintReport($request)
    {
        $args = new stdClass();
        $args->sender = $request->sender;
        $args->fileName = $request->fileName;

        return $this->checkEventResult($this->onPrintReport, $args);
    }

    public $onBeginExportReport = null;

    private function invokeBeginExportReport($request)
    {
        $args = new stdClass();
        $args->sender = $request->sender;
        $args->settings = $request->settings;
        $args->format = $request->format;
        $args->fileName = $request->fileName;

        return $this->checkEventResult($this->onBeginExportReport, $args);
    }

    public $onEndExportReport = null;

    private function invokeEndExportReport($request)
    {
        $args = new stdClass();
        $args->sender = $request->sender;
        $args->format = $request->format;
        $args->fileName = $request->fileName;
        $args->data = $request->data;

        return $this->checkEventResult($this->onEndExportReport, $args);
    }

    public $onEmailReport = null;

    private function invokeEmailReport($request)
    {
        $settings = new StiEmailSettings();
        $settings->to = $request->settings->email;
        $settings->subject = $request->settings->subject;
        $settings->message = $request->settings->message;
        $settings->attachmentName = $request->fileName.'.'.$this->getFileExtension($request->format);

        $args = new stdClass();
        $args->sender = $request->sender;
        $args->settings = $settings;
        $args->format = $request->format;
        $args->fileName = $request->fileName;
        $args->data = base64_decode($request->data);

        return $this->checkEventResult($this->onEmailReport, $args);
    }

    public $onDesignReport = null;

    private function invokeDesignReport($request)
    {
        $args = new stdClass();
        $args->sender = $request->sender;
        $args->fileName = $request->fileName;

        return $this->checkEventResult($this->onDesignReport, $args);
    }

//--- Methods

    public function registerErrorHandlers()
    {
        set_error_handler(function ($errNo, $errStr, $errFile, $errLine) {
            $result = StiResult::error("[".$errNo."] ".$errStr." (".$errFile.", Line ".$errLine.")");
            StiResponse::json($result);
        });

        register_shutdown_function(function () {
            $err = error_get_last();
            if ($err && (($err["type"] & E_COMPILE_ERROR) || ($err["type"] & E_ERROR) || ($err["type"] & E_CORE_ERROR) || ($err["type"] & E_RECOVERABLE_ERROR))) {
                $result = StiResult::error("[".$err["type"]."] ".$err["message"]." (".$err["file"].", Line ".$err["line"].")");
                StiResponse::json($result);
            }
        });
    }

    public function process($response = true)
    {
        $result = $this->innerProcess();
        if ($response) {
            StiResponse::json($result);
        }

        return $result;
    }


//--- Private methods

    private function createConnection($args)
    {
        switch ($args->database) {
            case StiDatabaseType::MYSQL:
                $connection = new StiMySqlAdapter();
                break;
//            case StiDatabaseType::MSSQL:
//                $connection = new StiDatabaseType();
//                break;
//            case StiDatabaseType::FIREBIRD:
//                $connection = new StiFirebirdAdapter();
//                break;
//            case StiDatabaseType::POSTGRESQL:
//                $connection = new StiPostgreSqlAdapter();
//                break;
//            case StiDatabaseType::ORACLE:
//                $connection = new StiOracleAdapter();
                break;
        }

        if (isset($connection)) {
            $connection->parse($args->connectionString);

            return StiResult::success(null, $connection);
        }

        return StiResult::error("Unknown database type [".$args->database."]");
    }

    private function innerProcess()
    {
        $request = new StiRequest();
        $result = $request->parse();
        if ($result->success) {
            switch ($request->event) {
                case StiEventType::BEGIN_PROCESS_DATA:
                case StiEventType::EXECUTE_QUERY:
                    $result = $this->invokeBeginProcessData($request);
                    if (!$result->success) {
                        return $result;
                    }
                    $queryString = $result->object->queryString;
                    $result = $this->createConnection($result->object);
                    if (!$result->success) {
                        return $result;
                    }
                    $connection = $result->object;
                    if (isset($queryString)) {
                        $result = $connection->execute($queryString);
                    } else {
                        $result = $connection->test();
                    }
                    $result = $this->invokeEndProcessData($request, $result);
                    if (!$result->success) {
                        return $result;
                    }
                    if (isset($result->object) && isset($result->object->result)) {
                        return $result->object->result;
                    }

                    return $result;

                case StiEventType::CREATE_REPORT:
                    return $this->invokeCreateReport($request);

                case StiEventType::OPEN_REPORT:
                    return $this->invokeOpenReport($request);

                case StiEventType::SAVE_REPORT:
                    return $this->invokeSaveReport($request);

                case StiEventType::SAVE_AS_REPORT:
                    return $this->invokeSaveReport($request);

                case StiEventType::PRINT_REPORT:
                    return $this->invokePrintReport($request);

                case StiEventType::BEGIN_EXPORT_REPORT:
                    return $this->invokeBeginExportReport($request);

                case StiEventType::END_EXPORT_REPORT:
                    return $this->invokeEndExportReport($request);

                case StiEventType::EMAIL_REPORT:
                    return $this->invokeEmailReport($request);

                case StiEventType::DESIGN_REPORT:
                    return $this->invokeDesignReport($request);
            }

            $result = StiResult::error("Unknown event [".$request->event."]");
        }

        return $result;
    }

    private function getFileExtension($format)
    {
        switch ($format) {
            case StiExportFormat::HTML:
            case StiExportFormat::HTML_5:
                return "html";

            case StiExportFormat::PDF:
                return "pdf";

            case StiExportFormat::EXCEL_2007:
                return "xlsx";

            case StiExportFormat::WORD_2007:
                return "docx";
        }

        return "";
    }
}
