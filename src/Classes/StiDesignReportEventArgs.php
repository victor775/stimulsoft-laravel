<?php

namespace SmartCats\Stimulsoft\Classes;

class StiDesignReportEventArgs
{
    public $fileName = null;

    public function __construct($fileName)
    {
        $this->fileName = $fileName;
    }
}
