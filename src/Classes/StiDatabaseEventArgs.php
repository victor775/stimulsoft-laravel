<?php

namespace SmartCats\Stimulsoft\Classes;

class StiDatabaseEventArgs
{
    public $sender = null;
    public $database = null;
    public $connectionInfo = null;
    public $queryString = null;

    public function __construct($sender, $database, $connectionInfo, $queryString = null)
    {
        $this->sender = $sender;
        $this->database = $database;
        $this->connectionInfo = $connectionInfo;
        $this->queryString = $queryString;
    }
}
