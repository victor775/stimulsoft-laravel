<?php

namespace SmartCats\Stimulsoft\Classes;

class StiDatabaseType
{
    const XML = "XML";
    const JSON = "JSON";
    const MYSQL = "MySQL";
    const MSSQL = "MS SQL";
    const POSTGRESQL = "PostgreSQL";
    const FIREBIRD = "Firebird";
    const ORACLE = "Oracle";
}
