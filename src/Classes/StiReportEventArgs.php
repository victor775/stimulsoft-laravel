<?php

namespace SmartCats\Stimulsoft\Classes;

class StiReportEventArgs
{
    public $sender = null;
    public $report = null;

    public function __construct($sender, $report = null)
    {
        $this->sender = $sender;
        $this->report = $report;
    }
}
