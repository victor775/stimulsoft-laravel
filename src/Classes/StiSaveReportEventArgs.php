<?php

namespace SmartCats\Stimulsoft\Classes;

class StiSaveReportEventArgs
{
    public $sender = null;
    public $report = null;
    public $fileName = null;

    public function __construct($report, $fileName)
    {
        $this->report = $report;
        $this->fileName = $fileName;
    }
}
