<?php

namespace SmartCats\Stimulsoft\Classes;

class StiEventType
{
    const EXECUTE_QUERY = "ExecuteQuery";
    const BEGIN_PROCESS_DATA = "BeginProcessData";
    //const EndProcessData = "EndProcessData";
    const CREATE_REPORT = "CreateReport";
    const OPEN_REPORT = "OpenReport";
    const SAVE_REPORT = "SaveReport";
    const SAVE_AS_REPORT = "SaveAsReport";
    const PRINT_REPORT = "PrintReport";
    const BEGIN_EXPORT_REPORT = "BeginExportReport";
    const END_EXPORT_REPORT = "EndExportReport";
    const EMAIL_REPORT = "EmailReport";
    const DESIGN_REPORT = "DesignReport";
}
