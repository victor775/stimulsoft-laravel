<?php

namespace SmartCats\Stimulsoft\Classes;

class StiExportFormat
{
    const HTML = "Html";
    const HTML_5 = "Html5";
    const PDF = "Pdf";
    const EXCEL_2007 = "Excel2007";
    const WORD_2007 = "Word2007";
}
