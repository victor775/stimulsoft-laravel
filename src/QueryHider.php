<?php

namespace SmartCats\Stimulsoft;

class QueryHider
{
    public static function hide($jsonReportString)
    {
        $json = json_decode($jsonReportString, true);

        if (empty($json['Dictionary']['DataSources'])) {
            return $jsonReportString;
        }

        foreach ($json['Dictionary']['DataSources'] as $key => $val) {
            if (isset($val['SqlCommand'])) {
                $queryName = "query_{$key}";

                $parameters = explode(';', $val['SqlCommand'], 2);

                if (isset($parameters[1])) {
                    $queryName .= ";$parameters[1]";
                }

                $json['Dictionary']['DataSources'][$key]['SqlCommand'] = $queryName;
            }
        }

        return json_encode($json);
    }

    public static function getMap($jsonReportString)
    {
        $queries = [];

        $json = json_decode($jsonReportString, true);

        if (empty($json['Dictionary']['DataSources'])) {
            return $queries;
        }

        foreach ($json['Dictionary']['DataSources'] as $key => $val) {
            if (isset($val['SqlCommand'])) {
                $queryName = "query_{$key}";

                $queries[$queryName] = static::getQueryFromString($val['SqlCommand']);
            }
        }

        return $queries;
    }

    public static function getQueryFromString($queryString)
    {
        $parameters = explode(';', $queryString, 2);

        return $parameters[0];
    }
}
